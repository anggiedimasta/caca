package com.example.anggie.citysmarthospital;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private final LatLng locationNow = new LatLng(-7.9762372, 112.6165876);
    private GoogleMap mMap;
    private int width;
    private boolean isSearchOpened = false;
    private DatabaseAccess databaseAccess;
    private boolean statusPelayananRS = true, statusPelayananPuskesmas = true;
    private SlidingUpPanelLayout slidingLayout;
    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
        }
    };
    private Marker lastOpened = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Point size = new Point();
        this.getWindowManager().getDefaultDisplay().getSize(size);
        width = size.x;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        String provider = service.getBestProvider(criteria, false);
//        Location location = service.getLastKnownLocation(provider);
//        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());

//        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        if (locationManager != null) {
//            boolean gpsIsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//            boolean networkIsEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//
//            if (gpsIsEnabled) {
//                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 10F, (android.location.LocationListener) this);
//            }
//            else if(networkIsEnabled)
//            {
//                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000L, 10F, (android.location.LocationListener) this);
//            }
//        }

        setUpMapIfNeeded();

        initSeekBar(navigationView);
        initSwitch(navigationView);

        slidingLayout = (SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        slidingLayout.setPanelHeight(340);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                // Check if there is an open info window
                if (lastOpened != null) {
                    // Close the info window
                    lastOpened.hideInfoWindow();
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

                    // Is the marker the same marker that was already open
                    if (lastOpened.equals(marker)) {
                        // Nullify the lastOpened object
                        lastOpened = null;
                        // Return so that the info window isn't opened again
                        return true;
//                    } else {
//                        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }

                // Open the info window for the marker
                // Re-assign the last opened such that we can close it later
                lastOpened = marker;
                marker.showInfoWindow();
                initSliderList(lastOpened);
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                // Event was handled by our code do not launch default behaviour.
                return true;
            }
        });
    }

    private void initSliderList(Marker markerNow) {
        final ListView listView = (ListView) findViewById(R.id.sliderListPelayanan);

        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        String alamat = databaseAccess.getAlamat(markerNow.getTitle());
        String telepon = databaseAccess.getTelepon(markerNow.getTitle());
        List<String> listSpesialisasi = databaseAccess.getSpesialisasi(markerNow.getTitle());
        String spesialisasi = "";
        for (String s : listSpesialisasi) {
            spesialisasi += "\n\t" + s ;
        }
        List<String> listFasilitas = databaseAccess.getFasilitas(markerNow.getTitle());
        String fasilitas = "";
        for (String s : listFasilitas) {
            fasilitas += "\n\t" + s ;
        }

        String[] values = new String[] { markerNow.getTitle(),
                alamat,
                telepon,
                "Spesialisasi: "+spesialisasi,
                "Fasilitas: "+fasilitas
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPos = position;
                String itemVal = (String) listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(),
                "Position: "+itemPos+" ListItem: "+itemVal, Toast.LENGTH_LONG)
                .show();
            }
        });
        databaseAccess.close();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null)
        {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setMyLocationEnabled(true);
//        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//            @Override
//            public void onMyLocationChange(Location location) {
//                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                if(mCircle == null || mMarker == null){
//                    drawMarkerWithCircle(latLng);
//                }else{
//                    updateMarkerWithCircle(latLng);
//                }
//            }
//        });

            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);

        Location myLocation  = mMap.getMyLocation();
//        LatLng locNow = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationNow, 17));
//        mMap.setOnMyLocationChangeListener(myLocationChangeListener);

            if (mMap != null)
            {
                mMap.setMyLocationEnabled(true);
            }

            databaseAccess = DatabaseAccess.getInstance(this);
            databaseAccess.open();
            List<String> namepel = databaseAccess.getName();
            List<Float> latit = databaseAccess.getLat();
            List<Float> longit = databaseAccess.getLong();
            for (int i = 0; i < namepel.size(); i++) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latit.get(i), longit.get(i)))
                        .title(namepel.get(i)));
            }
            databaseAccess.close();
        }
    }

//    private void updateMarkerWithCircle(LatLng position) {
//        mCircle.setCenter(position);
//        mMarker.setPosition(position);
//    }
//
//    private void drawMarkerWithCircle(LatLng position){
//        double radiusInMeters = 100.0;
//        int strokeColor = 0xffff0000; //red outline
//        int shadeColor = 0x44ff0000; //opaque red fill
//
//        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
//        mCircle = mMap.addCircle(circleOptions);
//
//        MarkerOptions markerOptions = new MarkerOptions().position(position);
//        mMarker = mMap.addMarker(markerOptions);
//    }

    private void handleMenuSearch(){
        if(isSearchOpened){ //test if the search is open
            isSearchOpened = false;
        } else { //open the search entry
            doSearch();
            isSearchOpened = true;
        }
    }

    private void doSearch() {
        Toast.makeText(this, "Searching for: ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            if(isSearchOpened) {
                handleMenuSearch();
            } else {
                drawer.closeDrawer(GravityCompat.START);
            }
        } else {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            MenuItem menu_bantuan = navigationView.getMenu().findItem(R.id.nav_bantuan);
            if (menu_bantuan.isChecked()) {
                TextView bantuan = (TextView) findViewById(R.id.text_bantuan);
                View map = findViewById(R.id.map);
                bantuan.setVisibility(View.GONE);
                map.setVisibility(View.VISIBLE);
                menu_bantuan.setChecked(false);
            } else {
                super.onBackPressed();
            }
        }
    }

    private int calculateZoomLevel(int km) {
        double equatorLength = 6378140; // in meters
        double widthInPixels = width;
        double metersPerPixel = (equatorLength * km) / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 2000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        return zoomLevel;
    }

    private void initSeekBar(NavigationView navigationView) {
        //seekbar
        SeekBar jangkauanControl = (SeekBar) navigationView.getMenu().findItem(R.id.nav_jangkauan_seekbar).getActionView().findViewById(R.id.jangkauan_bar);
        final TextView textProgress = (TextView) navigationView.getMenu().findItem(R.id.nav_jangkauan_seekbar).getActionView().findViewById(R.id.text_progress);
        jangkauanControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            int progressChanged = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                progressChanged = progress;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationNow, calculateZoomLevel(31 - progress)));
                String setT = "Jangkauan: " + String.valueOf(progress) + " Km";
                textProgress.setText(setT);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(MainActivity.this, "seek bar progress:" + progressChanged,
//                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initSwitch(final NavigationView navigationView) {
        //switch
        final SwitchCompat switchLayananRumahSakit = (SwitchCompat) navigationView.getMenu().findItem(R.id.nav_jenis_rumah_sakit).getActionView().findViewById(R.id.onoff);
        final SwitchCompat switchLayananPuskesmas = (SwitchCompat) navigationView.getMenu().findItem(R.id.nav_jenis_puskesmas).getActionView().findViewById(R.id.onoff);
        final MenuItem menu_jangkauan = navigationView.getMenu().findItem(R.id.nav_jangkauan);
        final MenuItem menu_jangkauan_seekbar = navigationView.getMenu().findItem(R.id.nav_jangkauan_seekbar);

        switchLayananRumahSakit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MenuItem menu_spesialisasi_umum = navigationView.getMenu().findItem(R.id.nav_spesialisasi_umum);
                MenuItem menu_spesialisasi_ibu_dan_anak = navigationView.getMenu().findItem(R.id.nav_spesialisasi_ibu_dan_anak);
                MenuItem menu_spesialisasi_bedah = navigationView.getMenu().findItem(R.id.nav_spesialisasi_bedah);
                if (isChecked) {
                    statusPelayananRS = true;
                    menu_spesialisasi_umum.setVisible(true);
                    menu_spesialisasi_ibu_dan_anak.setVisible(true);
                    menu_spesialisasi_bedah.setVisible(true);
                    menu_jangkauan.setVisible(true);
                    menu_jangkauan_seekbar.setVisible(true);
                } else {
                    statusPelayananRS = false;
                    menu_spesialisasi_umum.setVisible(false);
                    menu_spesialisasi_ibu_dan_anak.setVisible(false);
                    menu_spesialisasi_bedah.setVisible(false);
                    if (!switchLayananPuskesmas.isChecked()) {
                        menu_jangkauan.setVisible(false);
                        menu_jangkauan_seekbar.setVisible(false);
                    }
                }
                pelayananHandle();
            }
        });
        switchLayananPuskesmas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MenuItem menu_spesialisasi_rawat_inap = navigationView.getMenu().findItem(R.id.nav_spesialisasi_rawat_inap);
                if (isChecked) {
                    statusPelayananPuskesmas = true;
                    menu_spesialisasi_rawat_inap.setVisible(true);
                    menu_jangkauan.setVisible(true);
                    menu_jangkauan_seekbar.setVisible(true);
                } else {
                    statusPelayananPuskesmas = false;
                    menu_spesialisasi_rawat_inap.setVisible(false);
                    if (!switchLayananPuskesmas.isChecked()) {
                        menu_jangkauan.setVisible(false);
                        menu_jangkauan_seekbar.setVisible(false);
                    }
                }
                pelayananHandle();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(getApplicationContext(), MainActivity.class)));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_search:
                handleMenuSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void pelayananHandle () {
        Marker myMarker;
        if (statusPelayananRS && statusPelayananPuskesmas) {
            databaseAccess.open();
            mMap.clear();
            List<String> namepel = databaseAccess.getName();
            List<Float> latit = databaseAccess.getLat();
            List<Float> longit = databaseAccess.getLong();
            for (int i = 0; i < namepel.size(); i++) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latit.get(i), longit.get(i)))
                        .title(namepel.get(i)));
            }
            databaseAccess.close();
        }
        else if (!statusPelayananRS && statusPelayananPuskesmas) {
            databaseAccess.open();
            mMap.clear();
            List<String> namepel = databaseAccess.getName(2);
            List<Float> latit = databaseAccess.getLat(2);
            List<Float> longit = databaseAccess.getLong(2);
            for (int i = 0; i < namepel.size(); i++) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latit.get(i), longit.get(i)))
                        .title(namepel.get(i)));
            }
            databaseAccess.close();
        }
        else if (statusPelayananRS && !statusPelayananPuskesmas) {
            databaseAccess.open();
            mMap.clear();
            List<String> namepel = databaseAccess.getName(1);
            List<Float> latit = databaseAccess.getLat(1);
            List<Float> longit = databaseAccess.getLong(1);
            for (int i = 0; i < namepel.size(); i++) {
                myMarker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latit.get(i), longit.get(i)))
                        .title(namepel.get(i)));
            }
            databaseAccess.close();
        }
        else if (!statusPelayananRS && !statusPelayananPuskesmas) {
            mMap.clear();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.nav_bantuan:
                TextView bantuan = (TextView) findViewById(R.id.text_bantuan);
                View map = findViewById(R.id.map);
                if (item.isChecked()) {
//                    Toast.makeText(this, "Clicked: Menu Bantuan", Toast.LENGTH_SHORT).show();
                    bantuan.setVisibility(View.GONE);
                    map.setVisibility(View.VISIBLE);
                    item.setChecked(false);
                } else {
                    bantuan.setVisibility(View.VISIBLE);
                    map.setVisibility(View.GONE);
                    item.setChecked(true);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
//        return true;
    }
}
