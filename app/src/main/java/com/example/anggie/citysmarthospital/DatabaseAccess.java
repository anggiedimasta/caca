package com.example.anggie.citysmarthospital;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anggi on 5/18/2016.
 */
public class DatabaseAccess {
    private static DatabaseAccess instance;
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new Database(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public List<String> getName() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT nama FROM tb_pelayanan", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<String> getName(int idJenisPelayanan) {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT nama FROM tb_pelayanan where id_jenis_pelayanan = "+idJenisPelayanan, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<Float> getLat() {
        List<Float> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT latitude FROM tb_pelayanan", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getFloat(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<Float> getLat(int idJenisPelayanan) {
        List<Float> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT latitude FROM tb_pelayanan where id_jenis_pelayanan = "+idJenisPelayanan, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getFloat(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<Float> getLong() {
        List<Float> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT longitude FROM tb_pelayanan", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getFloat(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<Float> getLong(int idJenisPelayanan) {
        List<Float> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT longitude FROM tb_pelayanan where id_jenis_pelayanan = "+idJenisPelayanan, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getFloat(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public String getAlamat(String namaPelayanan) {
        Cursor cursor = database.rawQuery("SELECT alamat FROM tb_pelayanan where nama = '"+namaPelayanan+"'", null);
        cursor.moveToFirst();
        String alamat = cursor.getString(0);
        cursor.close();
        return alamat;
    }
    public String getTelepon(String namaPelayanan) {
        Cursor cursor = database.rawQuery("SELECT telepon FROM tb_pelayanan where nama = '"+namaPelayanan+"'", null);
        cursor.moveToFirst();
        String telepon = cursor.getString(0);
        cursor.close();
        return telepon;
    }
    public List<String> getSpesialisasi(String namaPelayanan) {
        int idPelayanan = getIdPelayanan(namaPelayanan);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT tb_spesialisasi.nama FROM tb_spesialisasi, tb_spesialisasi_pelayanan " +
                "where tb_spesialisasi_pelayanan.id_pelayanan = "+idPelayanan+
                " and tb_spesialisasi_pelayanan.id_spesialisasi = tb_spesialisasi.id", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public List<String> getFasilitas(String namaPelayanan) {
        int idPelayanan = getIdPelayanan(namaPelayanan);
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT tb_fasilitas.nama FROM tb_fasilitas, tb_fasilitas_pelayanan " +
                "where tb_fasilitas_pelayanan.id_pelayanan = "+idPelayanan+
                " and tb_fasilitas_pelayanan.id_fasilitas = tb_fasilitas.id", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public int getIdPelayanan(String namaPelayanan) {
        Cursor cursor = database.rawQuery("SELECT id FROM tb_pelayanan where nama = '"+namaPelayanan+"'", null);
        cursor.moveToFirst();
        int id = cursor.getInt(0);
        cursor.close();
        return id;
    }
//    public List<Integer> getJenisPelayanan() {
//        List<Integer> list = new ArrayList<>();
//        Cursor cursor = database.rawQuery("SELECT")
//    }
}
