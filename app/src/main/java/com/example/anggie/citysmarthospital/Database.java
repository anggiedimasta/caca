package com.example.anggie.citysmarthospital;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by anggi on 5/18/2016.
 */
public class Database extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "cure.db";
    private static final int DATABASE_VERSION = 1;

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
